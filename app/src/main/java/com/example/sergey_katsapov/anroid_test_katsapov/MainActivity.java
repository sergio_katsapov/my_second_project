package com.example.sergey_katsapov.anroid_test_katsapov;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;



public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState)

    {
        super.onCreate(savedInstanceState);
        setContentView(new DrawView(this));
    }

    //прорисовка робота
    class Circle {
        public double x; // абсцисса центра
        public double y; // ордината центра
        public double r; // радиус



        public void printCircle() {
            System.out.println("Окружность с центром (" + x + ";" + y + ") и радиусом " + r);
            return;
        }
        public void moveCircle(double a, double b) {
            x = x + a;
            y = y + b;
        }

        public void zoomCircle(double r) {
            this.r = this.r * r;
        }

        public Circle() {
            x = 0.0;
            y = 0.0;
            r = 1.0;
        }

        public Circle(double a, double b, double s) {
            x = a;
            y = b;
            r = s;
        }

        // метод вычисляющий площадь круга
        public double squareCircle() {
            double s = Math.PI * r * r;
            return s;
        }
    }




    class DrawView extends View {
        Paint p;
        RectF rectf;
        Circle o1 = new Circle(1,-1,14);
        float[] points;
        float[] points2;
        Path path;
        //Path path2;
        //Path path1;
        //double[] x;
        //double[] y;

        public DrawView(Context context) {
            super(context);
            p = new Paint();
            path = new Path();
            rectf = new RectF(700, 100, 800, 150);
            points = new float[]{1000,1600,1000, 1300, 1000,1300, 800, 1300, 800, 1300, 800,1100,800,1100,1000,1100,1000,1100,1000,400, 1000, 400};
            points2 = new float[]{1000,100,1000,200, 1000, 100, 100, 100,100,100, 100,150,100,150, 300,150,300,150,300,300,300,300,50,300,50,300,50,500,50,500,100,500,100,500, 100, 800,100,800, 50, 800, 50, 800, 50,1000,50,1000,50,1200,50,1200,100,1200,100,1200,100,1600,100,1600,700,1600};

            o1.printCircle();

        }


        protected void onDraw(Canvas canvas) {

            canvas.drawColor(Color.DKGRAY);

            p.setColor(Color.BLACK);
            p.setStrokeWidth(10);

            // рисуем точки их массива points
            canvas.drawLines(points, p);
            canvas.drawLines(points2, p);


           // path.addCircle(900, 1550, 25, Path.Direction.CW);
            p.setColor(Color.BLUE);
            canvas.drawPath(path, p);
            Circle o2 = new Circle(1,-1,14);
            o2.printCircle();

        }


    }


        class Point {
            private final double x;
            private final double y;

            public Point(double x, double y) {
                this.x = x;
                this.y = y;
            }

            public double getX() {
                return x;
            }

            public double getY() {
                return y;
            }


//        @Override
//        protected void onDraw(Canvas canvas) {
//            canvas.drawColor(Color.DKGRAY);
//
//            // очистка path
//            path.reset();
//
//            // угол
//            path.moveTo(1000, 1600);
//            path.lineTo(1000, 1300);
//            path.lineTo(800, 1300);
//            path.lineTo(800, 1100);
//            path.lineTo(1000, 1100);
//            path.lineTo(1000, 300);
//
//            p.setColor(Color.WHITE);
//            canvas.drawPath(path, p);

//
//            path1.moveTo(1000,100);
//            //path1.lineTo(1000, 300);
//            path1.lineTo(1000, 100);
//            path1.lineTo(100, 100);
//            path1.lineTo(100, 150);
//            path1.lineTo(300, 150);
//            path1.lineTo(300, 300);
//            path1.lineTo(50, 300);
//            path1.lineTo(50, 500);
//            path1.lineTo(100, 500);
//            path1.lineTo(100, 800);
//            path1.lineTo(50, 800);
//            path1.lineTo(50, 1000);
//            path1.lineTo(50, 1200);
//            path1.lineTo(100, 1200);
//            path1.lineTo(100, 1600);
//            path1.lineTo(700, 1600);

//
//           /// квадрат внутри склада(колонна)
//            path.moveTo(700, 800);
//            path.lineTo(800, 800);
//            path.lineTo(800, 400);
//            path.lineTo(700, 400);
//
//            path.close();
//
//
//            path2.addCircle(900, 1550, 25, Path.Direction.CW);
//            p.setColor(Color.BLUE);
//            canvas.drawPath(path2, p);
//
//            path2.close();
//
//             //квадрат и круг
//            //path.addRect(rectf, Path.Direction.CW);
//            //path.addCircle(450, 150, 25, Path.Direction.CW);
//
//            // рисование path1
//            p.setColor(Color.WHITE);
//            canvas.drawPath(path1, p);
//
//
//            // очистка path1
//            path1.reset();
//
//           /* // две пересекающиеся линии
//            path1.moveTo(50,50);
//            path1.lineTo(500,250);
//            path1.moveTo(500,50);
//            path1.lineTo(50,250);
//
//            // рисование path1
//            p.setColor(Color.GREEN);
//            canvas.drawPath(path1, p);*/
//
//
//            /*// добавление path1 к path
//            path.addPath(path1);*/
//
//           // смещение
//            //path.offset(500,100);
//
//            //рисование path
//            //p.setColor(Color.BLUE);
//            canvas.drawPath(path, p);
//        }

        }

    }
